import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        while (true){
            int Stertype = InputType();
            if (Stertype == 5){
                System.out.print("Bye bye!!!");
                break;
            }
            int number = InputNumber();
            process(number, Stertype); 
        }
    }

    static int InputType() {
        Scanner Num = new Scanner(System.in);
        while (true) {
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            int Startype = Num.nextInt();
            if (Startype >= 1 && Startype <= 5) {
                return Startype;
            } else {
                System.out.println("Error: Please input number between 1-5");

            }
        }
    }

    static int InputNumber() {
        Scanner Num = new Scanner(System.in);
            System.out.print("Please input number: ");
            int number = Num.nextInt();
            return number;
    }

    static void process(int number, int Startype) {
        switch (Startype) {
            case 1:
            for (int i = 0;i < number;i++){
                for (int j = 0;j <= i;j++){
                    System.out.print("*");
                }
                    System.out.println();
                }
                    break;
            case 2:
                for (int i = number; i > 0; i--) {
                    for (int j = 0; j < i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                } break;
            case 3:
                for (int i = 0; i < number; i++) {
                    for (int j = 0; j <= number; j++) {
                        if (j > i) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                } break;
            case 4:
                for (int i = number + 1; i > 0; i--) {
                    for (int j = 0; j < number + 1; j++) {
                        if (j < i) {
                            System.out.print(" ");
                        } else {
                            System.out.print("*");
                        }
                    }
                    System.out.println();    
                } break;
        }
    }

}
