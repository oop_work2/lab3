import java.util.Scanner;
public class Problem5 {
    public static void main(String[] args) {
        Scanner text = new Scanner (System.in);
        String txt;
        while (true) {
            System.out.print("Please input: ");
            txt = text.nextLine();
            if (txt.equalsIgnoreCase("bye")) {
                System.out.println("Exit Program");
                System.exit(0);
            }
            System.out.println(txt);
        }
    }
}
